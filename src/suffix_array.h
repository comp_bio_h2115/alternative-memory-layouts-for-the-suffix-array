#ifndef SUFFIX_ARRAY
#define SUFFIX_ARRAY
#include <vector>
#include <utility>
#include <cassert>
#include <bits/stdc++.h>

using namespace std;

/* Helper functions */
int string_compare(const char *text, const char *pattern, int pattern_length);

/* Sorted layout functions */
int binary_search_pattern(string &pattern, string &text, int *suffix_array);
int simple_accelerant_compare(string &text, string &pattern, int *suffix_array);
int lower_bound_sorted(string &text, string &pattern, int *suffix_array);
int upper_bound_sorted(string &text, string &pattern, int *suffix_array);

/* Eytzinger layout functions */
vector<int> build_binary_search_tree(int *suffix_array, int text_length);
int BST_search_pattern(vector<int> &bst, string &text, string &pattern, int *suffix_array);
int simple_accelerant_bst_compare(vector<int> &bst, string &text, string &pattern, int *suffix_array);
int BST_search_multiple_occurence(vector<int> &bst, string &text, string &pattern, int *suffix_array);

/* B-tree layout functions */
template<typename T>
class MyComparator
{
public:
    bool operator()(const T& a, const T& b) {
        return false;
    }
};


/* Unused functions */
// int *build_suffix_array(std::string &text, int string_length, int *suffix_array);
// std::pair <std::vector<int>, std::vector<int> > precomputeLcps(vector<int> &lcp);

#endif
