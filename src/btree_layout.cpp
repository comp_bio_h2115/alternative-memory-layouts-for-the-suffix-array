#include "suffix_array.h"
// #include "btree.hpp"
using namespace std;

// void build_BTREE(int *suffix_array, vector<long> &btree, int low, int high, int index, int order)
// {
//     if (low > high) {
//         return;
//     }

//     long start = low + (high - low) / order;

//     /*cout << "Low: " << low << "     High: " << high << "    Mid: " << mid;
//     cout << "    Index: " << index << "    Element: " << suffix_array[mid] << endl;*/

//     for (int i = start; i < high; i += order) {
//         btree[index] = suffix_array[start];
//         build_BTREE(suffix_array, btree, low, )
//     }

//     for (int i = 0; i < order; ++i) {
//         build_BTREE(suffix_array, bst, low, mid - 1, 2 * index + 1, order);
//     }

//     /* Compute the left child of the root. */
//     build_BST(suffix_array, bst, low, mid - 1, 2 * index + 1);

//     build_BST(suffix_array, bst, mid + 1, high, 2 * index + 2);
// }

// vector<long> build_binary_search_tree(int *suffix_array, int text_length) {
//     //long *bst = new long[text_length - 1];
//     vector<long> bst(text_length * 2, -1);

//     build_BST(suffix_array, bst, 0, text_length - 1, 0);

//     return bst;
// }

// long BST_search_pattern(vector<long> &bst, string &text, string &pattern, int *suffix_array)
// {
//     long index = 0;
//     long text_length = text.length();
//     clock_t start = clock();

//     while (index < 2 * text_length) {
//         long cmp_result = string_compare(text.c_str() + bst[index], pattern.c_str(), pattern.length());
//         /*cout << "\nString: " << text.c_str() + bst[index];
//         cout << "\nPattern: " << pattern.c_str();
//         cout << "\nCmp: " << cmp_result;*/

//         if (cmp_result == 0) {
//             cout << "BST Time taken: " << (clock() - start) / (double)CLOCKS_PER_SEC << endl;
//             return bst[index];
//         }
//         else if (cmp_result < 0) {
//             index <<= 1;
//             index += 2;
//         }
//         else {
//             index <<= 1;
//             ++index;
//         }
//     }

//     cout << "BST Time taken: " << (clock() - start) / (double)CLOCKS_PER_SEC << endl;
//     return -1;
// }

// long simple_accelerant_bst_compare(vector<long> &bst, string &text,
//                                     string &pattern, int *suffix_array)
// {
//     long index, left_lcp, right_lcp, matched, pat_len, text_len;
//     const char *ctext = text.c_str();
//     const char *cpattern = pattern.c_str();
 
//     index = left_lcp = right_lcp = 0;
//     text_len = text.length();
//     pat_len = pattern.length();

//     clock_t start = clock();

//     while (index < 2 * text_len) {
//         matched = min(left_lcp, right_lcp);
//         const char *tmp = ctext + bst[index];

//         while ((matched < pat_len) && cpattern[matched] == tmp[matched])
//             ++matched;

//         if (pat_len == matched) {
//             cout << "BST accelerant Time taken: " << (clock() - start) / (double)CLOCKS_PER_SEC << endl;
//             return bst[index];
//         }

//         if (cpattern[matched] > tmp[matched]) {
//             /* Move right */
//             index <<= 1;
//             index += 2;
//             left_lcp = matched;
//         } else {
//             /* Move left */
//             index <<= 1;
//             ++index;
//             right_lcp = matched;
//         }
//     }

//     cout << "BST accelerant Time taken: " << (clock() - start) / (double)CLOCKS_PER_SEC << endl;
//     return -1;
// }
