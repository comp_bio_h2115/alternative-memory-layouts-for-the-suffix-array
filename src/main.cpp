#include <iostream>
#include <string>
#include <string.h>
#include <fstream>
#include <divsufsort.h>
#include "suffix_array.h"
#include "btree.hpp"

/* The number of text patterns to be read. */
#define NUM_OF_STRINGS      500000

/* The number of test files */
#define NUM_OF_TESTCASES    4

#define NUM_OF_LAYOUTS      3
#define NUM_OF_OPERATIONS   3
#define OUTPUT_PRECISION    10

#define SORTED              0
#define EYTZINGER           1
#define BTREE               2

/* Node will have (BTREE_ORDER - 1) items */
#define BTREE_ORDER			17

#define OP_SEARCH           0
#define OP_SIMP_ACC         1
#define OP_MULTI_SEARCH     2

using namespace std;

const unsigned short DESC_WIDTH = 80;
const unsigned short TIME_WIDTH = 15;

/* The time taken by a given operation of a given layout */
double time_span [NUM_OF_LAYOUTS][NUM_OF_OPERATIONS];

/*
 * Different testcase files
 * valid_test_cases     - Contains matching patterns
 * invalid_test_cases   - Contains mismatch patterns
 */
string testcases[NUM_OF_TESTCASES] = {
    "../datasets/valid_test_cases",
    "../datasets/invalid_test_cases",
    "../datasets/multiple.txt",
    "../datasets/valid_invalid_cases",
};

string testcase_desc[NUM_OF_TESTCASES] = {
    "Matching pattern strings",
    "Mismatch pattern strings",
    "Includes multiple occurences",
    "Includes mix of all types of inputs"
};

/* This method helps to print a given string as centered alignment */
void center_print(const char *str, unsigned short width)
{
    unsigned short mid = 0;
    int len = strlen(str);

    mid = (width - len) / 2;
    cout << string(mid, ' ') << str << string(mid, ' ');
}

void print_header()
{
    string description("Task completed");
    string time("Time (in sec)");

    center_print(description.c_str(), DESC_WIDTH);
    cout << "|";

    center_print(time.c_str(), TIME_WIDTH);
    cout << endl;
}

void print_description(char *display_string)
{
    cout << left << setw(DESC_WIDTH) << display_string << "|";
}

void print_line_separator()
{
    cout << string(DESC_WIDTH + TIME_WIDTH + 5, '-') << endl;
}

void print_time(double time)
{
    cout << right << setw(TIME_WIDTH) << setprecision(OUTPUT_PRECISION) << time << endl;
    print_line_separator();
}

int main() {
	bool is_text = true;
	string line, text_name, text;
	int *suffix_array, *suff;
	int text_length = 0, num_of_strings = 0;
	vector<int> lcp_array;
	int res, res1, res2;
	clock_t start, end;
    int num_of_patterns = 0;

	start = clock();
	text = "";
	while (getline(cin, line) && num_of_strings < NUM_OF_STRINGS) {
		is_text = !is_text;
		if (!is_text) {
			text_name = line;
			continue;
		}

		text += line;
		text += '$';
		num_of_strings++;
	}

	end = clock();
	string pattern("GTCATTACTTGTTTCAAGAGCGTT");

    print_header();
    print_line_separator();

    char output[DESC_WIDTH];
    sprintf(output, "Finished reading %d lines", num_of_strings);
    print_description(output);
    print_time((end - start) / ((double) CLOCKS_PER_SEC));

	text_length = text.length();
	suffix_array = new int[text_length];

	start = clock();
	divsufsort((unsigned char *)text.c_str(), suffix_array, text_length);
	end = clock();

    /* Suffix Tree Construction */
    sprintf(output, "Build Suffix Tree");
    print_description(output);
    print_time((end - start) / (double) CLOCKS_PER_SEC);

    /* B-Tree Construction */
	start = clock();
	Tree <int, BTREE_ORDER, MyComparator<int> > tree(suffix_array, suffix_array + text_length);
	end = clock();
    sprintf(output, "Built B-Tree");
    print_description(output);
    // cout << endl;
    // cout << "btree simple_accelerant_compare : " << tree.simple_accelerant_compare(text, pattern) << endl;
    // cout << "btree range : " << tree.get_range(text, pattern) << endl;
    print_time((end - start) / (double) CLOCKS_PER_SEC);
    // tree.disp_like_tree();

    /* Eytzinger Layout Construction */
	start = clock();
	vector<int> eytzinger_bst = build_binary_search_tree(suffix_array, text_length);
	end = clock();
    sprintf(output, "Built Eytzinger Layout");
    print_description(output);
    print_time((end - start) / (double) CLOCKS_PER_SEC);

	for (int i = 0; i < NUM_OF_TESTCASES; ++i) {
        string pattern;
        std::ifstream testcase_file(testcases[i].c_str());
        cout << endl;
        
        sprintf(output, "Iteration-%d - %s", i + 1, testcase_desc[i].c_str());
        center_print(output, DESC_WIDTH);
        cout << endl;
        print_line_separator();

        num_of_patterns = 0;

        int multimatch_eytzinger = 0;
        int multimatch_btree = 0;
        int multimatch_simple = 0;

        while (getline(testcase_file, pattern)) {
            num_of_patterns++;

            /* Binary Search in a Sorted Layout */
    		start = clock();
    		res = binary_search_pattern(pattern, text, suffix_array);
    		end = clock();
            time_span[SORTED][OP_SEARCH] += (end - start) / (double) CLOCKS_PER_SEC;

            /* Simple Accelerant Search in a Sorted Layout */
    		start = clock();
    		res = simple_accelerant_compare(text, pattern, suffix_array);
    		end = clock();
            time_span[SORTED][OP_SIMP_ACC] += (end - start) / (double) CLOCKS_PER_SEC;

            /* Multi-Occurence Search in a Sorted Layout */
    		start = clock();
    		res1 = lower_bound_sorted(text, pattern, suffix_array);
    		res2 = upper_bound_sorted(text, pattern, suffix_array);
    		end = clock();
            time_span[SORTED][OP_MULTI_SEARCH] += (end - start) / (double) CLOCKS_PER_SEC;
            multimatch_simple += (res2 - res1);

            /* Simple Compare in B-Tree */
            start = clock();
            res = BST_search_pattern(eytzinger_bst, text, pattern, suffix_array);
            end = clock();
            time_span[EYTZINGER][OP_SEARCH] += (end - start) / (double) CLOCKS_PER_SEC;

            /* Simple Acceleratn Search in B-Tree */
            start = clock();
            res = simple_accelerant_bst_compare(eytzinger_bst, text, pattern, suffix_array);
            end = clock();
            time_span[EYTZINGER][OP_SIMP_ACC] += (end - start) / (double) CLOCKS_PER_SEC;

            /* Multi-Occurence Search in B-Tree */
            start = clock();
            res = BST_search_multiple_occurence(eytzinger_bst, text, pattern, suffix_array);
            end = clock();
            time_span[EYTZINGER][OP_MULTI_SEARCH] += (end - start) / (double) CLOCKS_PER_SEC;
            multimatch_eytzinger += res;

            /* Simple Compare in B-Tree */
            start = clock();
            res = tree.simple_compare(text, pattern);
            end = clock();
            time_span[BTREE][OP_SEARCH] += (end - start) / (double) CLOCKS_PER_SEC;

            /* Simple Accelerant search in B-Tree */
            start = clock();
      		res = tree.simple_accelerant_compare(text, pattern);
            end = clock();
            time_span[BTREE][OP_SIMP_ACC] += (end - start) / (double) CLOCKS_PER_SEC;

            /* Multi-Occurence Search in B-Tree */
            start = clock();
	    	res = tree.get_range(text, pattern);
            end = clock();
            time_span[BTREE][OP_MULTI_SEARCH] += (end - start) / (double) CLOCKS_PER_SEC;
            multimatch_btree += res;
        }

        /* Printing results. */
        sprintf(output, "Simple Compare in Sorted Layout");
        print_description(output);
        print_time(time_span[SORTED][OP_SEARCH] / num_of_patterns);

        sprintf(output, "Simple Accelerant Search in Sorted Layout");
        print_description(output);
        print_time(time_span[SORTED][OP_SIMP_ACC] / num_of_patterns);

        sprintf(output, "Multi-Occurence Search in Sorted Layout");
        print_description(output);
        cout << endl;
        sprintf(output, "Multimatches: %d", multimatch_simple);
        print_description(output);
        print_time(time_span[SORTED][OP_MULTI_SEARCH] / num_of_patterns);

        sprintf(output, "Simple Compare in Eytzinger Layout");
        print_description(output);
        print_time(time_span[EYTZINGER][OP_SEARCH] / num_of_patterns);

        sprintf(output, "Simple Accelerant Search in Eytzinger Layout");
        print_description(output);
        print_time(time_span[EYTZINGER][OP_SIMP_ACC] / num_of_patterns);

        sprintf(output, "Multi-Occurence Search in Eytzinger Layout");
        print_description(output);
        cout << endl;
        sprintf(output, "Multimatches: %d", multimatch_eytzinger);
        print_description(output);
        print_time(time_span[EYTZINGER][OP_MULTI_SEARCH] / num_of_patterns);

        sprintf(output, "Simple Compare in B-Tree Layout");
        print_description(output);
        print_time(time_span[BTREE][OP_SEARCH] / num_of_patterns);

        sprintf(output, "Simple Accelerant Search in B-Tree Layout");
        print_description(output);
        print_time(time_span[BTREE][OP_SIMP_ACC] / num_of_patterns);

        sprintf(output, "Multi-Occurence Search in B-Tree Layout");
        print_description(output);
        cout << endl;
        sprintf(output, "Multimatches: %d", multimatch_btree);
        print_description(output);
        print_time(time_span[BTREE][OP_MULTI_SEARCH] / num_of_patterns);
   	}

	cout << endl << endl;
	return 0;
}
