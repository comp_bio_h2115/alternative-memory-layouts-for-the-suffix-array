#include "suffix_array.h"
using namespace std;

void build_BST(int *suffix_array, vector<int> &bst, int low, int high, int index)
{
    if (low > high) {
        return;
    }

    int mid = low + (high - low) / 2;
    bst[index] = suffix_array[mid];

    build_BST(suffix_array, bst, low, mid - 1, 2 * index + 1);

    build_BST(suffix_array, bst, mid + 1, high, 2 * index + 2);
}

vector<int> build_binary_search_tree(int *suffix_array, int text_length) {
    vector<int> bst(text_length * 2, -1);

    build_BST(suffix_array, bst, 0, text_length - 1, 0);

    return bst;
}

int BST_search_pattern(vector<int> &bst, string &text, string &pattern, int *suffix_array)
{
    int index = 0;
    int text_length = text.length();

    while (index < 2 * text_length) {
        int cmp_result = string_compare(text.c_str() + bst[index], pattern.c_str(), pattern.length());

        if (cmp_result == 0) {
            return bst[index];
        }
        else if (cmp_result < 0) {
            index <<= 1;
            index += 2;
        }
        else {
            index <<= 1;
            ++index;
        }
    }

    return -1;
}

int BST_search_multiple(vector<int> &bst, string &text,
                            string &pattern, int *suffix_array,
                            int index)
{
    int text_length = text.length();

    while (index < 2 * text_length) {
        int cmp_result = string_compare(text.c_str() + bst[index], pattern.c_str(), pattern.length());

        if (cmp_result == 0) {
            return 1 + BST_search_multiple(bst, text, pattern, suffix_array, (index << 1) + 1) +
                BST_search_multiple(bst, text, pattern, suffix_array, (index << 2) + 2); 
        }
        else if (cmp_result < 0) {
            index <<= 1;
            index += 2;
        }
        else {
            index <<= 1;
            ++index;
        }
    }

    return 0;
}

int BST_search_multiple_occurence(vector<int> &bst, string &text,
                                    string &pattern, int *suffix_array)
{
    return BST_search_multiple(bst, text, pattern, suffix_array, 0);
}

int simple_accelerant_bst_compare(vector<int> &bst, string &text,
                                    string &pattern, int *suffix_array)
{
    int index, left_lcp, right_lcp, matched, pat_len, text_len;
    const char *ctext = text.c_str();
    const char *cpattern = pattern.c_str();
 
    index = left_lcp = right_lcp = 0;
    text_len = text.length();
    pat_len = pattern.length();

    clock_t start = clock();

    while (index < 2 * text_len) {
        matched = min(left_lcp, right_lcp);
        const char *tmp = ctext + bst[index];

        while ((matched < pat_len) && cpattern[matched] == tmp[matched])
            ++matched;

        if (pat_len == matched) {
            return bst[index];
        }

        if (cpattern[matched] > tmp[matched]) {
            /* Move right */
            index <<= 1;
            index += 2;
            left_lcp = matched;
        } else {
            /* Move left */
            index <<= 1;
            ++index;
            right_lcp = matched;
        }
    }

    return -1;
}
