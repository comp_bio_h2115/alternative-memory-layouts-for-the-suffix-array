#include "suffix_array.h"
using namespace std;

/*
 * Returns:
 * 0  - if match
 * +1 - if text > pattern
 * -1 - if text < pattern
 */
int string_compare(const char *text, const char *pattern, int pattern_length) {
    int index = 0;

    while (text[index] == pattern[index] &&
        index < pattern_length) {   
        index++;
    }

    if (index == pattern_length) index = 0;
    else if (text[index] < pattern[index]) index = -1;
    else index = 1;

    return index;
}

int lower_bound_sorted(string &text, string &pattern, int *suffix_array)
{

    int left, left_lcp, right_lcp, matched, pat_len, start_index, cur;
    bool move_left;
    int it, step, first = 0;

    const char *ctext = text.c_str();
    const char *cpattern = pattern.c_str();
 
    left = left_lcp = right_lcp = 0;
    pat_len = pattern.length();

    int count = text.length();
 
    while (count > 0) {
        cur = left;
        step = count / 2;
        cur += step;

        move_left = true;
        matched = min(left_lcp, right_lcp);
        // matched = 0;
        start_index = suffix_array[cur];

        // std::advance(it, step);

        while ((matched < pat_len) && (ctext[start_index + matched] == cpattern[matched]))
            ++matched;

        if (cpattern[matched] <= ctext[start_index + matched])
            move_left = false;

        if (move_left) {
            left = cur + 1;
            count -= step + 1;
            right_lcp = matched;
        } else {
            count = step;
            left_lcp = matched;
        }

    }

    return left;
}


int upper_bound_sorted(string &text, string &pattern, int *suffix_array)
{
    int left, left_lcp, right_lcp, matched, pat_len, start_index, cur;
    int it, step, first = 0;

    const char *ctext = text.c_str();
    const char *cpattern = pattern.c_str();

    // cout << "text : " << ctext << endl; 
    left = left_lcp = right_lcp = 0;
    pat_len = pattern.length();

    int count = text.length();
 
    while (count > 0) {
        cur = left;
        step = count / 2;
        cur += step;

        matched = min(left_lcp, right_lcp);
        // matched = 0;
        start_index = suffix_array[cur];

        // std::advance(it, step);
        // cout << "count " << count << " step " << step << " cur " << cur << " left " << left << endl;

        while ((matched < pat_len) && (ctext[start_index + matched] == cpattern[matched]))
            ++matched;

        if (!(cpattern[matched] < ctext[start_index + matched]) || matched == pat_len) {
            left = cur + 1;
            count -= step + 1;
            right_lcp = matched;
        } else {
            count = step;
            left_lcp = matched;
        }

    }

    return left;
}

int simple_accelerant_compare(string &text, string &pattern, int *suffix_array)
{
    int left, right, mid, left_lcp, right_lcp, matched, pat_len, text_len, start_index;
    const char *ctext = text.c_str();
    const char *cpattern = pattern.c_str();

    left = left_lcp = right_lcp = 0;
    text_len = right = text.length();
    pat_len = pattern.length();

    while (left <= right) {
        mid = left + (right - left) / 2;
        matched = min(left_lcp, right_lcp);
        start_index = suffix_array[mid];

        while ((matched < pat_len) && (ctext[start_index + matched] == cpattern[matched]))
            ++matched;

        if (matched == pat_len)
            return start_index;

        // cout << "matching " << ctext[start_index + matched] << " with " << cpattern[matched] << endl;
        if (ctext[start_index + matched] < cpattern[matched]) {
            // if (mid == right - 1)
                // return -1;
            left = mid + 1;
            left_lcp = matched;
        } else {
            // if (mid == left + 1)
                // return -1;
            right =  mid - 1;
            right_lcp = matched;
        }
    }

    // cout << ctext << "," << cpattern << "Came here\n";
    return -1;
}

int binary_search_pattern(string &pattern, string &text, int *suffix_array)
{
    int size = text.length();
    int low = 0;
    int high = size - 1;
    int mid;
    int k = 0;
    int pattern_length = pattern.length();

    clock_t start = clock();

    while (low <= high) {
        mid = low + (high - low) / 2;
        /*cout << "Low : " << low << endl;
        cout << "Mid : " << mid << endl;
        cout << "High: " << high << endl;
        cout << "---------------------" << endl;*/

        //k = strcmp(text.c_str() + suffix_array[mid], pattern.c_str());
        k = string_compare(text.c_str() + suffix_array[mid],
                            pattern.c_str(), pattern_length);

        if (k == 0) {
            return suffix_array[mid];
        }
        /* Pattern is larger. Move right.*/
        else if (k < 0) {
            low = mid + 1;
        }
        else {
            high = mid - 1;
        }
    }

    return -1;
}

